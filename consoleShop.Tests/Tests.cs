using consoleShopLib.Data;
using consoleShopLib.Helpers;
using consoleShopLib.Models;
using consoleShopLib.Services;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace consoleShop.Tests
{
    [TestFixture]
    public class Tests
    {
        ShopContext _context;
        IdentityService _identityService;
        ProductService _productService;
        UserService _userService;
        OrderService _orderService;

        [SetUp]
        public void Setup()
        {
            _context = ShopContext.GetInstance();
            _identityService = new IdentityService(_context, new ConsoleLogger<IdentityService>());
            _productService = new ProductService(_context, new ConsoleLogger<ProductService>());
            _userService = new UserService(_context, new ConsoleLogger<UserService>());
            _orderService = new OrderService(_context, new ConsoleLogger<OrderService>());
                
        }
        private static IEnumerable<TestCaseData> NewProductTestCases
        {
            get
            {
                yield return new TestCaseData(
                    new Product() {
                        Id = "testId",
                        Category = "testCategory1",
                        Description = "testDescription1",
                        Name = "testName1",
                        Price = 1234.44M,
                        Supplier = "testSupplier1"
                    }
                    );
            }
        }


        [Test]
        public void InitializeWasSuccessfull()
        {
            Assert.IsNotNull(_context);
            Assert.IsNotNull(_identityService);
            Assert.IsNotNull(_productService);
            Assert.IsNotNull(_userService);
            Assert.IsNotNull(_orderService);
        }
        [TestCase(new int[] { 3,3,3})]
        public void SeedDataWasSuccessfull(int [] countExpected)
        {
            Assert.AreEqual(_context.Users.Count, countExpected[0]);
            Assert.AreEqual(_context.Products.Count, countExpected[1]);
            Assert.AreEqual(_context.Orders.Count, countExpected[2]);
        }
        [TestCase(3)]
        public void GetProductsReturnsProducts(int expectedCount)
        {
            Assert.AreEqual(_productService.GetProducts().Count, expectedCount);
        }
        [Test]
        public void GetProductById()
        {
            var product = _context.Products[0];
            var id = product.Id;
            Assert.AreEqual(product,_productService.GetById(id));
        }
        [TestCase("product1")]
        public void GetProductByName(string productName)
        {
            var product = _context.Products[0];
            var actual = _productService.FindProduct(productName);
            Assert.AreEqual(product, actual.FirstOrDefault());
        }
        [TestCase(2)]
        public void AddProductToBucket(int expectedBucketCount)
        {
            var user = _context.Users[0];
            var userId = user.Id;
            var products = _context.Products.Take(expectedBucketCount).ToList();
            _productService.AddProductToBucket(userId, products);
            Assert.Multiple(() =>
            {
                Assert.AreEqual(_context.Users[0].Bucket.Count, expectedBucketCount);
                Assert.AreEqual(_productService.ShowBucket(_context.Users[0].Id).Count, expectedBucketCount);

            });
                
        }
        [TestCaseSource("NewProductTestCases")]
        public void AddProduct(Product product1)
        {
            _productService.AddProduct(product1);
            Assert.Contains(product1, _context.Products);
            _productService.DeleteProduct(product1.Id);
        }

        [TestCaseSource("NewProductTestCases")]
        public void EditProduct(Product product1)
        {
            var temp = new Product()
            {
                Name = _context.Products[0].Name,
                Id = _context.Products[0].Id,
                Category = _context.Products[0].Category,
                Description = _context.Products[0].Description,
                Supplier = _context.Products[0].Supplier,
                Price = _context.Products[0].Price
            };

            var id = _context.Products[0].Id;
            product1.Id = id;
            _productService.EditProduct(id,product1);
            var expected = _context.Products[0];

            Assert.Multiple(() =>
            {
                Assert.AreEqual(product1.Category, expected.Category);
                Assert.AreEqual(product1.Description, expected.Description);
                Assert.AreEqual(product1.Id, expected.Id);
                Assert.AreEqual(product1.Name, expected.Name);
                Assert.AreEqual(product1.Price, expected.Price);
                Assert.AreEqual(product1.Supplier, expected.Supplier);
                Assert.AreEqual(product1.ToString(), expected.ToString());

            });
            _context.Products[0] = temp;
        }

    }
}