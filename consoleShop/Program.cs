﻿using consoleShopLib.Data;
using consoleShopLib.Helpers;
using consoleShopLib.Models;
using consoleShopLib.Services;
using System;

namespace consoleShop
{
    static class Program
    {
        static void InitShop()
        {
            var context = ShopContext.GetInstance();

            var shopHolder = new ShopHolder(
                new IdentityService(context, new ConsoleLogger<IdentityService>()),
                new ProductService(context, new ConsoleLogger<ProductService>()),
                new UserService(context, new ConsoleLogger<UserService>()),
                new OrderService(context,new ConsoleLogger<OrderService>())
                );

            shopHolder.ShowMenu();
        }
        static void Main(string[] args)
        {
            InitShop();
        }
    }
}
