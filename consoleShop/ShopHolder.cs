﻿using consoleShopLib.Data;
using consoleShopLib.Models;
using consoleShopLib.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Linq;

namespace consoleShop
{
    public class ShopHolder
    {
        public User User { get; set; }
        private readonly IIdentityService _identityService;
        private readonly IProductService _productService;
        private readonly IUserService _userService;
        private readonly IOrderService _orderService;

        public ShopHolder(
            IIdentityService identityService,
            IProductService productService,
            IUserService userService,
            IOrderService orderService)
        {
            User = new User() { Role = Roles.Guest }; 

            _identityService = identityService;
            _productService = productService;
            _userService = userService;
            _orderService = orderService;

        }
        private void PrintActionsForGuest()
        {
            SameActionsForAll();
            Console.WriteLine("Enter 3 to register");
            Console.WriteLine("Enter 4 to login");
            
        }
        private void SameActionsForAll()
        {
            Console.WriteLine("Enter -1 to showMenu");

            Console.WriteLine("Enter 1 to list products");
            Console.WriteLine("Enter 2 to find product by name");
            Console.WriteLine("Enter 15 to get current user");
            Console.WriteLine("Enter 99 to exit program");
        }

        private void PrintActionsForRegisteredUser()
        {
            SameActionsForGuestAndRegistered();
            Console.WriteLine("Enter 6 to get order history");
            Console.WriteLine("Enter 7 to set order status GOT");
            Console.WriteLine("Enter 8 to change personal info");   
        }
        private void SameActionsForGuestAndRegistered()
        {
            SameActionsForAll();

            Console.WriteLine("Enter 3 to add products to bucket");
            Console.WriteLine("Enter 4 to finish order or cancel order");
            Console.WriteLine("Enter 5 to cancel order");

            Console.WriteLine("Enter 19 to log out");
            Console.WriteLine("Enter 27 to show bucket");


        }
        private void PrintActionsForAdmin()
        {
            SameActionsForGuestAndRegistered();
            Console.WriteLine("Enter 6 to get all users");
            Console.WriteLine("Enter 7 to change user info");
            Console.WriteLine("Enter 8 to add new product");
            Console.WriteLine("Enter 9 to change product info");
            Console.WriteLine("Enter 10 to list orders");
            Console.WriteLine("Enter 11 to change order status");
        }

        public void ShowMenu()
        {
            int selection = 0;
            Console.WriteLine("Enter -1 to showMenu");
            while (selection != 99)
            {

                if (User.Role == Roles.Guest)
                {
                    ProcessActionsForGuest(selection);
                }
                else if (User.Role == Roles.RegisteredUser)
                {
                    ProcessActionsForRegisteredUser(selection);
                }
                else if (User.Role == Roles.Admin)
                {
                    ProcessActionsForAdmin(selection);
                }

                Console.WriteLine("Please select an action");
                string act = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(act) &&
                    int.TryParse(act, out int res))
                {   
                    selection = res;
                }
                else
                {
                    Console.WriteLine("incorrect input");
                }
                


            }
        }
        public void ProcessActionsForGuest(int selection)
        {
            switch (selection)
            {
                case -1:
                    PrintActionsForGuest();
                    break;
                case 1:
                    GetProducts();
                    break;
                case 2:
                    FindProductByName();
                    break;
                case 3:
                    RegisterUser();
                    break;
                case 4:
                    LoginUser();
                    break;
                case 15:
                    GetCurrentUser();
                    break;
            }
        }
        private (string,bool) ReadConsole()
        {
            Console.WriteLine("Enter esc to return");
            var data = Console.ReadLine();
            if (data.Equals("esc"))
            {
                return (data, true);
            }
            return (data,false);
        }

        private void LoginUser()
        {
            bool returnNeed;
            Console.WriteLine("Enter login data");

            Console.WriteLine("Enter user name");
            string userName;
            (userName, returnNeed) = ReadConsole();
            if (returnNeed) return;

            Console.WriteLine("Enter Password");
            string Password;
            (Password, returnNeed) = ReadConsole();
            if (returnNeed) return;

            var user =  _identityService.Login(userName, Password);
            if (user!=null)
            {
                User = user;
            }

        }

        private void RegisterUser()
        {
            Console.WriteLine("Enter registration data");

            Console.WriteLine("Enter user name");
            string userName = Console.ReadLine();

            Console.WriteLine("Enter phone number");
            string PhoneNumber = Console.ReadLine();

            Console.WriteLine("Enter Email");
            string Email = Console.ReadLine();

            Console.WriteLine("Enter Password");
            string Password = Console.ReadLine();

            Console.WriteLine("Enter amount of money");
            string MoneyStr = Console.ReadLine();
            decimal Money = 0.0M;
            if (!string.IsNullOrWhiteSpace(MoneyStr) &&
                decimal.TryParse(MoneyStr,out decimal res))
            {
                Money = res;
            }
            else
            {
                Console.WriteLine("Incorrect data");
                return;
            }
                

            var user = new User()
            {
                Id = Guid.NewGuid().ToString(),
                Name = userName,
                PhoneNumber = PhoneNumber,
                Email = Email,
                Password = Password,
                Money = Money,
                Role = Roles.RegisteredUser
            };

            if (_identityService.Register(user))
            {
                User = user;
            }
        }

        private void FindProductByName()
        {
            Console.WriteLine("Enter product name");
            string productName = Console.ReadLine();
            _productService.FindProduct(productName).ForEach(p =>
            {
                Console.WriteLine(p.ToString());
            });
        }

        private void GetProducts()
        {
            Console.WriteLine("Getting products...");

            _productService.GetProducts().ForEach(p => {
                Console.WriteLine(p.ToString());
            });
        }

        private void ProcessActionsForRegisteredUser(int selection)
        {
            switch (selection)
            {
                case -1:
                    PrintActionsForRegisteredUser();
                    break;
                case 1:
                    GetProducts();
                    break;
                case 2:
                    FindProductByName();
                    break;
                case 3:
                    AddProductToBucket();
                    break;
                case 4:
                    CreateNewOrder();
                    break;
                case 5:
                    CancelOrder();
                    break;
                case 6:
                    ShowOrderHistory();
                    break;
                case 7:
                    SetOrderStatusToGot();
                    break;
                case 8:
                    UpdatePersonalUserInfo();
                    break;
                case 19:
                    LogOut();
                    break;
                case 15:
                    GetCurrentUser();
                    break;
                case 27:
                    ShowUserBucket();
                    break;
            }
        }

        private void ShowUserBucket()
        {
            if (User.Bucket != null)
            {
                User.Bucket.ToList().ForEach(p => { Console.WriteLine(p); });
                return;
            }
            else
            {
                var productsInBucket = _productService.ShowBucket(User.Id);
                if (productsInBucket != null)
                {
                    productsInBucket.ForEach(p => { Console.WriteLine(p); });
                    return;
                }
                
            }
            Console.WriteLine($"{User} \n Empty bucket");
                
           
        }

        private void GetCurrentUser()
        {
            Console.WriteLine("Current user");
            Console.WriteLine(User.ToString());
        }

        private void LogOut()
        {
            
            User = new User() { Role = Roles.Guest };
            Console.WriteLine("Logged out");
        }

        private void UpdatePersonalUserInfo()
        {
            Console.WriteLine("Enter new personal data");

            Console.WriteLine($"Enter user name {User.Name}");
            string userName = Console.ReadLine();

            Console.WriteLine($"Enter phone number {User.PhoneNumber}");
            string PhoneNumber = Console.ReadLine();

            Console.WriteLine($"Enter Email {User.Email}");
            string Email = Console.ReadLine();

            Console.WriteLine($"Enter Password {User.Password}");
            string Password = Console.ReadLine();

            Console.WriteLine($"Enter amount of money {User.Money}");
            string MoneyStr = Console.ReadLine();
            decimal Money = 0.0M;
            if (!string.IsNullOrWhiteSpace(MoneyStr) &&
                decimal.TryParse(MoneyStr, out decimal res))
            {
                Money = res;
            }
            else
            {
                Console.WriteLine("Incorrect data");
                return;
            }

            var user = new User()
            {
                Name = userName,
                PhoneNumber = PhoneNumber,
                Email = Email,
                Password = Password,
                Money = Money,
                Role = Roles.RegisteredUser
            };
            User = user;
            _userService.EditPersonalInfo(User.Id,user,User);
        }

        private void SetOrderStatusToGot()
        {
            bool returnNeed;

            Console.WriteLine("Enter order id");
            string orderId;
            (orderId, returnNeed) = ReadConsole();
            if (returnNeed) return;

            _orderService.SetStatusGotForOrder(User.Id,orderId);

        }

        private void ShowOrderHistory()
        {
            Console.WriteLine("Order history");
            _orderService.UserOrdersHistory(User.Id).ForEach(o=>Console.WriteLine(o));
        }

        private void CancelOrder()
        {
            bool returnNeed;

            Console.WriteLine("Enter order id");
            string orderId;
            (orderId, returnNeed) = ReadConsole();
            if (returnNeed) return;

            _orderService.CancelOrder(User.Id, orderId);
        }

        private void AddProductToBucket()
        {
            Console.WriteLine("Adding products to bucket");
            bool returnNeed;

            string productId = "";
            var products = new List<Product>();
            while (productId != "none")
            {
                Console.WriteLine("Enter product id or none to break");
                (productId, returnNeed) = ReadConsole();
                if (returnNeed) return;
                if (productId.Equals("none")) break;
                var product = _productService.GetById(productId);
                if (product != null)
                {
                    products.Add(product);
                }
            }

            _productService.AddProductToBucket(User.Id, products);
        }

        private void CreateNewOrder()
        {
            Console.WriteLine("Creating new order");
            
            Console.WriteLine("Enter location");
            bool returnNeed;
            string location;
            (location, returnNeed) = ReadConsole();
            if (returnNeed) return;
            _orderService.MakeOrder(User.Id, location);
        }

        private void ProcessActionsForAdmin(int selection)
        {

            switch (selection)
            {
                case -1:
                    PrintActionsForAdmin();
                    break;
                case 1:
                    GetProducts();
                    break;
                case 2:
                    FindProductByName();
                    break;
                case 3:
                    AddProductToBucket();
                    break;
                case 4:
                    CreateNewOrder();
                    break;
                case 5:
                    CancelOrder();
                    break;
                case 19:
                    LogOut();
                    break;
                case 6:
                    GetAllUsers();
                    break;
                case 7:
                    UpdateUserInfo();
                    break;
                case 8:
                    AddNewProduct();
                    break;
                case 9:
                    UpdateProduct();
                    break;
                case 10:
                    ListAllOrders();
                    break;
                case 11:
                    ChangeOrderStatus();
                    break;
                case 27:
                    ShowUserBucket();
                    break;


            }
        }

        private void ChangeOrderStatus()
        {
            Console.WriteLine("Changing order status");
            Console.WriteLine("Enter order id");
            
            bool returnNeed;
            string orderId;
            (orderId, returnNeed) = ReadConsole();
            if (returnNeed) return;

            Console.WriteLine("Choose order status from statuses below");

            Console.WriteLine("Enter 1 to set CanceledByAdmin");
            Console.WriteLine("Enter 2 to set Payed");
            Console.WriteLine("Enter 3 to set Sent");
            Console.WriteLine("Enter 4 to set Got");
            Console.WriteLine("Enter 5 to set Completed");
            string statusChoice;
            (statusChoice, returnNeed) = ReadConsole();
            if (returnNeed) return;
            int statusChoiceNum = 0;
            if(int.TryParse(statusChoice, out int res)){
                statusChoiceNum = res;
            }
            else
            {
                Console.WriteLine("Incorrect input");
                return;
            }

            OrderStatus orderStatus = OrderStatus.New;
            switch (statusChoiceNum)
            {
                case 1:
                    orderStatus = OrderStatus.CanceledByAdmin;
                    break;
                case 2:
                    orderStatus = OrderStatus.Payed;
                    break;
                case 3:
                    orderStatus = OrderStatus.Sent;
                    break;
                case 4:
                    orderStatus = OrderStatus.Got;
                    break;
                case 5:
                    orderStatus = OrderStatus.Completed;
                    break;
                default:
                    orderStatus = OrderStatus.New;
                    break;
            }

            _orderService.SetOrderStatus(orderId, orderStatus);
        }

        private void ListAllOrders()
        {
            Console.WriteLine("List all orders");
            _orderService.GetOrders().ForEach(o =>
            {
                Console.WriteLine(o);
            });
        }

        private void UpdateProduct()
        {
            Console.WriteLine("Enter product id");
            string productId;
            bool returnNeed;
            (productId, returnNeed) = ReadConsole();
            if (returnNeed) return;

            var product = _productService.GetById(productId);
            if (product == null)
            {
                Console.WriteLine("No such product");
                return;
            }

            Console.WriteLine($"Updating product \n {product}");

            var productUpdated = FillProductInfo();
            if (productUpdated == null) return;

            _productService.EditProduct(product.Id,productUpdated);
        }
        private Product FillProductInfo()
        {
            Console.WriteLine("Enter Product Name");
            bool returnNeed;
            string Name;
            (Name, returnNeed) = ReadConsole();
            if (returnNeed) return null;

            Console.WriteLine("Enter Product Supplier");
            string Supplier;
            (Supplier, returnNeed) = ReadConsole();
            if (returnNeed) return null;

            Console.WriteLine("Enter Product Price");
            string PriceStr;
            (PriceStr, returnNeed) = ReadConsole();
            if (returnNeed) return null;
            decimal Price = 0.0M;
            if (!string.IsNullOrWhiteSpace(PriceStr) &&
                decimal.TryParse(PriceStr, out decimal res))
            {
                Price = res;
            }
            else
            {
                Console.WriteLine("Incorrect data");
                return new Product();
            }


            Console.WriteLine("Enter Product Description");
            string Description;
            (Description, returnNeed) = ReadConsole();
            if (returnNeed) return null;

            Console.WriteLine("Enter Product Category");
            string Category;
            (Category, returnNeed) = ReadConsole();
            if (returnNeed) return null;

            var product = new Product()
            {
                Name = Name,
                Supplier = Supplier,
                Price = Price,
                Description = Description,
                Category = Category
            };
            return product;
        }
        private void AddNewProduct()
        {
            Console.WriteLine("Creating new product");
            var product = FillProductInfo();
            if (product == null) return;
            _productService.AddProduct(product);
        }

        private void UpdateUserInfo()
        {
            Console.WriteLine("Enter user Id");
            bool returnNeed;
            string userId;
            (userId, returnNeed) = ReadConsole();
            if (returnNeed) return;

            var user = _userService.GetById(userId);
            if(user == null)
            {
                Console.WriteLine("No such user");
                return;
            }

            Console.WriteLine("Enter new personal data");

            Console.WriteLine($"Enter user name {user.Name}");

            string userName;
            (userName, returnNeed) = ReadConsole();
            if (returnNeed) return;

            Console.WriteLine($"Enter phone number {user.PhoneNumber}");

            string PhoneNumber;
            (PhoneNumber, returnNeed) = ReadConsole();
            if (returnNeed) return;

            Console.WriteLine($"Enter Email {user.Email}");

            string Email;
            (Email, returnNeed) = ReadConsole();
            if (returnNeed) return;

            Console.WriteLine($"Enter Password {user.Password}");

            string Password;
            (Password, returnNeed) = ReadConsole();
            if (returnNeed) return;


            Console.WriteLine($"Enter amount of money {user.Money}");
            string MoneyStr = Console.ReadLine();
            decimal Money = 0.0M;
            if (!string.IsNullOrWhiteSpace(MoneyStr) &&
                decimal.TryParse(MoneyStr, out decimal res))
            {
                Money = res;
            }
            else
            {
                Console.WriteLine("Incorrect data");
                return;
            }

            var updatedUser = new User()
            {
                Name = userName,
                PhoneNumber = PhoneNumber,
                Email = Email,
                Password = Password,
                Money = Money,
                Role = Roles.RegisteredUser
            };

            _userService.EditPersonalInfo(userId, updatedUser, User);
        }

        private void GetAllUsers()
        {
            Console.WriteLine("all Users");
            _userService.GetUsers().ForEach(u =>
            {
                Console.WriteLine(u.ToString());
            });
        }
    }


}
