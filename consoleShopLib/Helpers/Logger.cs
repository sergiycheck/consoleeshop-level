﻿using System;
using System.Collections.Generic;
using System.Text;

namespace consoleShopLib.Helpers
{
    public interface ILogger<T> where T : class
    {
        public void Log(string message);
    }
    public class ConsoleLogger<T> : ILogger<T> where T : class
    {
        public void Log(string message)
        {
            Console.WriteLine($"invoking from {typeof(T)}");
            Console.WriteLine(message);
        }
    }
}
