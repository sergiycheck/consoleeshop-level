﻿using consoleShopLib.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace consoleShopLib.Data
{
    public class DataInitializer
    {
        public void Initialize(ShopContext context)
        {
            var users = new User[]
            {
                new User()
                {
                    PhoneNumber = "608-851-0227",
                    Name = "admin",
                    Email = "admin@domain.com",
                    Id = Guid.NewGuid().ToString(),
                    Password = "admin",
                    Money = 99999999.99M,
                    Role = Roles.Admin,
                },
                new User()
                {
                    PhoneNumber = "812-739-2213",
                    Name = "user1",
                    Email = "user1@domain.com",
                    Id = Guid.NewGuid().ToString(),
                    Password = "user1password",
                    Money = 1234.23M,
                    Role = Roles.RegisteredUser,
                },
                new User()
                {
                    PhoneNumber = "314-531-1033",
                    Name = "user2",
                    Email = "user2@domain.com",
                    Id = Guid.NewGuid().ToString(),
                    Password = "user2password",
                    Money = 3234.23M,
                    Role = Roles.RegisteredUser,
                }
            };
            context.Users = new List<User>();
            context.Users.AddRange(users);

            var products = new Product[]
            {
                new Product()
                {
                    Name = "product1",
                    Id = Guid.NewGuid().ToString(),
                    Price = 331.3M,
                    Supplier = "supplier1",
                    Category = "category1",
                    Description = "description1"
                },
                new Product()
                {
                    Name = "product2",
                    Id = Guid.NewGuid().ToString(),
                    Price = 111.3M,
                    Supplier = "supplier2",
                    Category = "category1",
                    Description = "description2"
                },
                new Product()
                {
                    Name = "product4",
                    Id = Guid.NewGuid().ToString(),
                    Price = 411.92M,
                    Supplier = "supplier2",
                    Category = "category2",
                    Description = "description3"
                }
            };
            context.Products = new List<Product>();
            context.Products.AddRange(products);


            var orders = new Order[]
            {
                new Order()
                {
                    Status = OrderStatus.Sent,
                    DateTime = DateTime.Parse("2018-09-01"),
                    Id = Guid.NewGuid().ToString(),
                    Location = "Missouri, Saint Louis, 1086  Blane Street",
                    Products = new List<Product>(){ products[0], products[1] },
                    Sum =products[0].Price+products[1].Price,
                    UserId = users[2].Id
                },
                new Order()
                {
                    Status = OrderStatus.New,
                    DateTime = DateTime.Parse("2018-09-02"),
                    Id = Guid.NewGuid().ToString(),
                    Location = "Kentucky, Bowling Green, 391  Crosswind Drive",
                    Products = new List<Product>(){ products[2], products[0] },
                    Sum =products[2].Price+products[0].Price,
                    UserId = users[1].Id
                },
                new Order()
                {
                    Status = OrderStatus.CanceledByAdmin,
                    DateTime = DateTime.Parse("2018-09-02"),
                    Id = Guid.NewGuid().ToString(),
                    Location = "Unreal",
                    Products = new List<Product>(){ products[2], products[0],products[1] },
                    Sum =products[2].Price+products[0].Price+products[1].Price,
                    UserId = users[2].Id
                }

            };
            context.Orders = new List<Order>();
            context.Orders.AddRange(orders);

        }
    }
}
