﻿using consoleShopLib.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace consoleShopLib.Data
{
    public class ShopContext
    {
        public List<User> Users { get; set; }
        public List<Order> Orders { get; set; }

        public List<Product> Products { get; set; }

        private static ShopContext instance;
        private static object syncRoot = new object();

        private ShopContext()
        {
            var dataInitializer = new DataInitializer();
            dataInitializer.Initialize(this);
        }
        public static ShopContext GetInstance()
        {
            Console.WriteLine($"Getting instance of ShopContext {DateTime.Now.TimeOfDay}");
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new ShopContext();
                }
            }
            return instance;
        }

    }
}
