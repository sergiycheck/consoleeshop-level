﻿using consoleShopLib.Data;
using consoleShopLib.Helpers;
using consoleShopLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace consoleShopLib.Services
{

    public interface IIdentityService
    {
        public User Login(string email, string password);
        public bool Register(User newUser);
        public bool DeleteAccount(string id);

    }
    public class IdentityService: IIdentityService
    {
        private readonly ShopContext _context;
        private readonly ILogger<IdentityService> _logger;
        public IdentityService(ShopContext context, ILogger<IdentityService> logger)
        {
            _context = context;
            _logger = logger;

        }
        public User Login(string email,string password)
        {
            //todo: validation for email,password admin

            var userLoggedIn =  _context.Users
                .Find(u => u.Email == email && u.Password == password);
            if (userLoggedIn == null)
            {
                _logger.Log($"Email {email} or password {password} is not correct");
                return null;
            }
            _logger.Log("User successfully logged-in");
            return userLoggedIn;
        }
        public bool Register(User newUser)
        {
            //todo: validation for email,password admin

            if (_context.Users.FirstOrDefault(u => u.Email == newUser.Email) != null)
            {
                _logger.Log($"Registration failed. User with such email {newUser.Email} already exists");
                return false;
            }
            _context.Users.Add(newUser);
            _logger.Log($"Successfully registered");
            return true;
        }
        public bool DeleteAccount(string id)
        {
            //todo: validation for  admin
            var userToRemove = _context.Users.FirstOrDefault(u => u.Id == id);

            if(userToRemove != null &&
                userToRemove.Orders!=null && 
                userToRemove.Orders.FirstOrDefault(o => o.Status == OrderStatus.New) != null)
            {
                _logger.Log($"Can't delete user {userToRemove}.\n Cancel or get your orders first");
                return false;
            }

            if (userToRemove != null)
            {
                _context.Users.Remove(userToRemove);
                _logger.Log($"user {userToRemove} Successfully deleted");
                return true;
            }
            _logger.Log($"Can't find user with such id {id}");
            return false;
        }
    }
}
