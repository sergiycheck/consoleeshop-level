﻿using consoleShopLib.Data;
using consoleShopLib.Helpers;
using consoleShopLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace consoleShopLib.Services
{
    public interface IProductService
    {
        public Product GetById(string productId);
        public List<Product> GetProducts();
        public List<Product> FindProduct(string name);
        public void AddProductToBucket(string userId, ICollection<Product> products);
        public void AddProduct(Product product);
        public void EditProduct(string productId, Product newProduct);
        public List<Product> ShowBucket(string userId);
        public bool DeleteProduct(string productId);


    }
    public class ProductService: IProductService
    {
        private readonly ShopContext _context;
        private readonly ILogger<ProductService> _logger;
        public ProductService(ShopContext context, ILogger<ProductService> logger)
        {
            _context = context;
            _logger = logger;
        }
        public Product GetById(string productId)
        {
            return _context.Products.FirstOrDefault(p => p.Id == productId);
        }
        public bool DeleteProduct(string productId)
        {
            var productToRemove = GetById(productId);
            if (productToRemove != null)
            {
                _context.Products.Remove(productToRemove);
                return true;
            }
            return false;

                
        }

        /// <summary>
        /// Only for admin
        /// </summary>
        public void AddProduct(Product product)
        {
            if(product!=null)
                _context.Products.Add(product);
        }

        public void AddProductToBucket(string userId, ICollection<Product> products)
        {
            var user = _context.Users.Find(u => u.Id == userId);
            if (user != null)
            {
                user.Bucket = products;
                _logger.Log($"Products was successfully add to {user.Name} bucket. Count {user.Bucket.Count} ");
            }
        }
        public List<Product> ShowBucket(string userId)
        {
            var user = _context.Users.Find(u => u.Id == userId);
            if (user != null && user.Bucket.Count>0)
            {
                return user.Bucket.ToList();
            }
            _logger.Log($"{userId} empty user bucket");
            return new List<Product>();
        }

        public void EditProduct(string productId, Product newProduct)
        {
            var product = _context.Products.Find(p => p.Id == productId);
            if (product != null)
            {
                product.Category = newProduct.Category;
                product.Description = newProduct.Description;
                product.Name = newProduct.Name;
                product.Price = newProduct.Price;
                product.Supplier = newProduct.Supplier;

                _logger.Log($"{product} \n Successfully updated");
                return;
            }
            _logger.Log($"Product doesn't exist");
        }

        public List<Product> FindProduct(string name)
        {
            return _context.Products.Where(p => p.Name.Contains(name)).ToList();
        }
        public List<Product> GetProducts()
        {
            return _context.Products;
        }


    }
}
