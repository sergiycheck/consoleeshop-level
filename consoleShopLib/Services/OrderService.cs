﻿using consoleShopLib.Data;
using consoleShopLib.Helpers;
using consoleShopLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace consoleShopLib.Services
{
    public interface IOrderService
    {
        public List<Order> GetOrders();
        public void MakeOrder(string userId, string location);
        public bool CancelOrder(string userId, string orderId);
        public bool SetOrderStatus(string orderId, OrderStatus status);
        public List<Order> UserOrdersHistory(string userId);
        public bool SetStatusGotForOrder(string userId, string orderId);
    }
    public class OrderService: IOrderService
    {
        private readonly ShopContext _context;
        private readonly ILogger<OrderService> _logger;
        public OrderService(ShopContext context, ILogger<OrderService> logger)
        {
            _context = context;
            _logger = logger;

        }

        public bool CancelOrder(string userId, string orderId)
        {
            var order = _context.Orders
                .Where(o => o.UserId == userId &&
                    o.Status != OrderStatus.Completed &&
                    o.Status != OrderStatus.CanceledByAdmin &&
                    o.Status != OrderStatus.Got)
                .FirstOrDefault(o => o.Id == orderId);
            if (order != null)
            {
                order.Status = OrderStatus.CanceledByUser;
                _logger.Log($"Order {order} successfully canceled");
                return true;
            }
            _logger.Log($"Can't cancel order with id {orderId}");
            return false;
        }

        public List<Order> GetOrders()
        {
            var orders = _context.Orders.ToList();
            if (orders != null)
                return orders;

            _logger.Log("No orders are preset");
            return new List<Order>();
        }

        public void MakeOrder(string userId, string location)
        {
            var user = _context.Users.Find(u => u.Id == userId);
            if (user != null && user.Bucket.Count > 0)
            {
                var sum = 0M;
                user.Bucket.ToList().ForEach(p =>
                {
                    sum += p.Price;
                });
                var order = new Order()
                {
                    DateTime = DateTime.Now,
                    Id = Guid.NewGuid().ToString(),
                    Location = location,
                    Status = OrderStatus.New,
                    Products = user.Bucket,
                    UserId = user.Id,
                    Sum = sum
                };

                _context.Orders.Add(order);

                user.Bucket.Clear();
            }
            else
            {
                _logger.Log("Cannot make order with empty bucket");

            }
        }
        /// <summary>
        /// only for admin
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool SetOrderStatus(string orderId, OrderStatus status)
        {
            var order = _context.Orders
                .FirstOrDefault(o => o.Id == orderId);

            if (order != null)
            {
                order.Status = status;
                _logger.Log($"Order {order} status was successfully set to {status}");
                return true;
            }
            _logger.Log($"Can't set order status with id {orderId}");
            return false;
        }

        public bool SetStatusGotForOrder(string userId, string orderId)
        {
            var order = _context.Orders
                .Where(o => o.UserId == userId &&
                    o.Status != OrderStatus.Completed &&
                    o.Status != OrderStatus.CanceledByAdmin &&
                    o.Status != OrderStatus.CanceledByUser)
                .FirstOrDefault(o => o.Id == orderId);

            if (order != null)
            {
                order.Status = OrderStatus.Got;
                _logger.Log($"Order {order} status was successfully set to GOT");
                return true;
            }
            _logger.Log($"Can't set order status with id {orderId}");
            return false;
        }

        public List<Order> UserOrdersHistory(string userId)
        {
            var user = _context.Users.Find(u => u.Id == userId);
            if (user != null)
            {
                var ordersHistory = _context.Orders.Where(o => o.UserId == userId).ToList();
                if (ordersHistory != null && ordersHistory.Count > 0)
                {
                    return ordersHistory;
                }
            }
            _logger.Log($"Empty user {userId} orders history");
            return new List<Order>();
        }
    }
}
