﻿using consoleShopLib.Data;
using consoleShopLib.Helpers;
using consoleShopLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace consoleShopLib.Services
{
    public interface IUserService
    {
        public void EditPersonalInfo(string userId, User newUserInfo, User currentUser);
        public List<User> GetUsers();
        public User GetById(string userId);

    }
    public class UserService: IUserService
    {
        private readonly ShopContext _context;
        private readonly ILogger<UserService> _logger;
        public UserService(ShopContext context, ILogger<UserService> logger)
        {
            _context = context;
            _logger = logger;

        }
        public User GetById(string userId) 
            => _context.Users.FirstOrDefault(u => u.Id == userId);

        public void EditPersonalInfo(string userId, User newUserInfo, User currentUser)
        {
            if(currentUser.Id==userId || currentUser.Role == Roles.Admin) 
            {
                var user = _context.Users.Find(u => u.Id == userId);
                if (user != null && newUserInfo != null)
                {
                    user.Email = newUserInfo.Email;
                    user.Name = newUserInfo.Name;
                    user.Password = newUserInfo.Password;
                    user.PhoneNumber = newUserInfo.PhoneNumber;
                    user.Money = newUserInfo.Money;//todo: di service to add money
                }
                return;
            }
            
        }
        
        /// <summary>
        /// Only for admin
        /// </summary>
        public List<User> GetUsers()
        {
            var users = _context.Users.ToList();
            if (users != null)
            {
                return users;
            }
            _logger.Log("No users");
            return new List<User>();
        }
        

    }
}
