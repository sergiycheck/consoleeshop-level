﻿using System;
using System.Collections.Generic;
using System.Text;

namespace consoleShopLib.Models
{
    public enum OrderStatus
    {
        New,
        CanceledByAdmin,
        Payed,
        Sent,
        Got,
        Completed,
        CanceledByUser
    }
    public class Order:BaseModel
    {
        public OrderStatus Status { get; set; } 
        public string Location { get; set; }
        public DateTime DateTime { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }

        public ICollection<Product>  Products { get; set; }
        public decimal Sum { get; set; }
        public override string ToString()
        {
            string count = Products != null ? Products.Count.ToString() : "empty";
            return $"{Id} \n Status =  {Status} \n {Location} \n {DateTime} {Sum}, owner {User.ToString() ?? "empty"}, \n products count {count}";
        }

    }
}
