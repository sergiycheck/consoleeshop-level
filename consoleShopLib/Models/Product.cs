﻿using System;
using System.Collections.Generic;
using System.Text;

namespace consoleShopLib.Models
{
    public class Product:BaseModel
    {
        public string Name { get; set; }
        public string Supplier { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public override string ToString()
        {
            return $" {Id} \n {Name} {Category} \n {Supplier} {Price} \n {Description}";
        }
    }
}
