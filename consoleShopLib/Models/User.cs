﻿using System;
using System.Collections.Generic;
using System.Text;

namespace consoleShopLib.Models
{
    public enum Roles
    {
        Guest,
        RegisteredUser,
        Admin
    }

    public class User:BaseModel
    {
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Roles Role { get; set; }
        public ICollection<Order> Orders { get; set; }
        public ICollection<Product> Bucket { get; set; }
        public decimal Money { get; set; }
        public override string ToString()
        {
            string count = Orders != null ? Orders.Count.ToString() : "empty";
            return $"{Id} \n {Name} {Role} \n {Email} {Password} \n {Money}, Orders count {count}";
        }
    }
}
